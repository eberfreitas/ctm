defmodule Ctm.Talks do
  @moduledoc """
  Módulo para guardar o estado das talks durante o processamento e com helpers
  para lidar com o input e realizar parsing inicial.
  """

  use Agent

  @doc """
  Inicializa o agent com a lista das talks.
  """
  def start_link(talks, opts \\ []) do
    default_opts = [name: Keyword.get(opts, :name, __MODULE__)]
    Agent.start_link(fn -> talks end, Keyword.merge(opts, default_opts))
  end

  @doc """
  Pega todas as talks do estado atual do agent.
  """
  def get(pid \\ __MODULE__) do
    Agent.get(pid, & &1)
  end

  @doc """
  Atualiza todas as talks no estado do agent.
  """
  def update(pid \\ __MODULE__, talks) do
    Agent.update(pid, fn _ -> talks end)
  end

  @doc """
  Converte input cru em uma lista de tuplas representando as talks onde cada
  tupla tem o seguinte formato: `{duracao_em_minutos, titulo_da_talk}`.
  """
  def make(block) when is_binary(block) do
    block
    |> String.split("\n")
    |> Enum.map(&String.trim/1)
    |> Enum.filter(fn l -> l != "" end)
    |> Enum.map(&assemble/1)
  end

  @doc false
  defp assemble(line) do
    {parse_minutes(line), line}
  end

  @doc false
  defp parse_minutes(line) do
    line
    |> String.split(" ")
    |> List.last()
    |> convert_minutes()
  end

  @doc false
  defp convert_minutes("lightning"), do: 5

  @doc false
  defp convert_minutes(minutes) do
    minutes
    |> String.replace("min", "")
    |> String.to_integer()
  end
end

defmodule Ctm.Orquestrator do
  @moduledoc """
  Módulo que une todas as peças (boundaries e talks) para gerar um output geral.
  """

  alias Ctm.{Queue, Talks}

  @doc """
  Recebe uma lista de boundaries onde cada item é uma tupla no seguinte formato:
  `{limite_minimo_em_minutos, limite_maximo_em_minutos}`. Também recebe uma
  lista de talks que pode ser gerada através da função `Ctm.Talks.make/1`.

  Vai inicializar o agent e rodar a função `Ctm.Queue.process/1` para cada par
  de boundaries informado, gerando a estrutura organizada de talks.
  """
  def run(boundaries, talks, agent_opts \\ []) do
    {:ok, pid} = Talks.start_link(talks, agent_opts)

    result =
      boundaries
      |> Enum.map(fn b ->
        state = %Queue{boundaries: b, talks: Talks.get(pid)}

        with {:ok, result} <- Queue.process(state) do
          Talks.update(pid, result.talks)
          result.sequence
        else
          {:error, reason} -> {:error, reason}
        end
      end)

    case result do
      {:error, reason} ->
        {:error, reason}

      _ ->
        if length(Talks.get()) != 0 do
          {:error, "O total de palestras excede o tempo do evento."}
        else
          result
        end
    end
  end

  @doc """
  Versão do `Ctm.Orquestrator.run` que levanta uma excessão se houver erro.
  """
  def run!(boundaries, talks, agent_opts \\ []) do
    case run(boundaries, talks, agent_opts) do
      {:error, reason} -> raise(reason)
      result -> result
    end
  end
end

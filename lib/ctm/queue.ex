defmodule Ctm.Queue do
  @moduledoc """
  Módulo com as funções de processamento de um segmento de palestras.
  """

  @upper 999_999

  defstruct boundaries: nil, talks: nil, sequence: [], retries: 0, duration: 0

  @doc """
  Realiza o processamento das talks. Recebe um struct `%Ctm.Queue{}` contendo
  os boundaries sendo utilizados e as talks a serem processadas. Itera sobre
  cada talk tentando encaixá-las da melhor maneira dentro dos boundaries
  definidos.
  """
  def process(%__MODULE__{talks: []} = state) do
    {min_minutes, max_minutes} = state.boundaries

    cond do
      state.duration == max_minutes ->
        {:ok, state}

      state.duration > min_minutes ->
        {:ok, state}

      true ->
        {:error, "O total das palestras não preenche o tempo mínimo do evento."}
    end
  end

  def process(%__MODULE__{talks: [_ | talks]} = state) do
    {min_minutes, max_minutes} = state.boundaries
    remaining_talks_count = length(talks)

    cond do
      state.retries > remaining_talks_count && min_minutes == max_minutes ->
        {:error, "Não é possível encaixar as palestras dentro do tempo de evento estabelecido."}

      state.retries > remaining_talks_count ->
        pick_next_fitting_talk(state)

      state.duration == max_minutes ->
        {:ok, state}

      true ->
        pick_next_talk(state)
    end
  end

  @doc false
  defp pick_next_talk(%__MODULE__{} = state) do
    %__MODULE__{talks: [talk | talks], boundaries: {_, max_minutes}} = state
    {minutes, _} = talk

    with {:ok, remaining_time} <- remaining_time(max_minutes - state.duration, minutes),
         :ok <- find_next_divider(remaining_time, talks) do
      sequence = state.sequence ++ [talk]
      duration = calculate_duration(sequence)

      process(%__MODULE__{
        state
        | talks: talks,
          sequence: sequence,
          retries: 0,
          duration: duration
      })
    else
      :error ->
        process(%__MODULE__{state | talks: talks ++ [talk], retries: state.retries + 1})
    end
  end

  @doc false
  defp pick_next_fitting_talk(%__MODULE__{} = state) do
    %__MODULE__{talks: original_talks, boundaries: {_, max_minutes}} = state

    possible_talks =
      original_talks
      |> Enum.filter(fn {min, _} -> min + state.duration <= max_minutes end)

    diff_talks = original_talks -- possible_talks

    case possible_talks do
      [] ->
        {:error,
         "O total de palestras excede o tempo do evento ou não existem palestras suficientes."}

      _ ->
        possible_talks =
          possible_talks
          |> Enum.sort(fn {curr, _}, {prev, _} -> curr >= prev end)

        [talk | talks] = possible_talks
        sequence = state.sequence ++ [talk]
        duration = calculate_duration(sequence)

        process(%__MODULE__{
          state
          | talks: talks ++ diff_talks,
            sequence: sequence,
            retries: 0,
            duration: duration
        })
    end
  end

  @doc false
  defp calculate_duration(sequence) do
    sequence
    |> Enum.reduce(0, fn {min, _}, acc -> acc + min end)
  end

  @doc false
  defp remaining_time(time, minutes) do
    case rem(time, minutes) do
      0 -> {:ok, time - minutes}
      _ -> :error
    end
  end

  @doc false
  defp find_next_divider(time, talks) do
    remainder =
      talks
      |> Enum.reduce(@upper, fn {min, _}, acc ->
        rem = rem(time, min)
        if rem < acc, do: rem, else: acc
      end)

    case remainder do
      0 -> :ok
      _ -> :error
    end
  end
end

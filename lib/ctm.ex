defmodule Ctm do
  @moduledoc """
  Entrypoint.
  """

  alias Ctm.{Orquestrator, Talks}

  @doc """
  Entry point da aplicação. Recebe o input, gera os dados e os organiza pra
  display na tela.
  """
  def main(_) do
    input = IO.read(:stdio, :all)
    talks = Talks.make(input)
    boundaries = [{180, 180}, {180, 180}, {180, 240}, {180, 240}]

    [track_one_morning, track_two_morning, track_one_afternoon, track_two_afternoon] =
      Orquestrator.run!(boundaries, talks)

    {:ok, morning_start_time} = Time.new(9, 0, 0)
    {:ok, afternoon_start_time} = Time.new(13, 0, 0)
    {:ok, lunch_time} = Time.new(12, 0, 0)
    {:ok, networking_time} = Time.new(17, 0, 0)

    track_one =
      make_track_seq(morning_start_time, track_one_morning) ++
        [{lunch_time, "Lunch"}] ++
        make_track_seq(afternoon_start_time, track_one_afternoon) ++
        [{networking_time, "Networking Event"}]

    track_two =
      make_track_seq(morning_start_time, track_two_morning) ++
        [{lunch_time, "Lunch"}] ++
        make_track_seq(afternoon_start_time, track_two_afternoon) ++
        [{networking_time, "Networking Event"}]

    IO.puts("Track 1:")
    print_track(track_one)
    IO.puts("")
    IO.puts("Track 2:")
    print_track(track_two)
  end

  @doc false
  defp print_track(track) do
    track
    |> Enum.each(fn {time, title} ->
      "#{format_time(time)} #{title}"
      |> IO.puts()
    end)
  end

  @doc false
  defp make_track_seq(start_time, talks) do
    hours =
      talks
      |> Enum.scan(start_time, fn {minutes, _}, time ->
        Time.add(time, minutes * 60)
      end)

    pairs = Enum.zip([start_time] ++ hours, talks)

    pairs
    |> Enum.map(fn {time, {_, title}} ->
      {time, title}
    end)
  end

  @doc false
  defp format_time(time) do
    {hour, ampm} = to_12hour_clock(time.hour)

    to_string(:io_lib.format("~2..0B:~2..0B~s", [hour, time.minute, ampm]))
  end

  @doc false
  defp to_12hour_clock(hour) do
    if hour > 12, do: {hour - 12, "PM"}, else: {hour, "AM"}
  end
end

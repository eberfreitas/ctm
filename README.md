# Conference Talk Manager

Aplicação de linha de comando que recebe uma lista de palestras e as encaixa
dentro de horários e tracks de um evento de maneira uniforme, dadas as
limitações de horário de cada período e cada track.

## Fazendo o build

Antes de qualquer coisa é necessário puxar as

A aplicação foi desenvolvida como um `escript`, de forma que para criar um build
basta executar:

```
$ mix escript.build
```

Um build da versão mais recente já está presente na raíz do projeto e se chama
`ctm`.

## Como usar

Basta enviar para a aplicação, via `stdin`, a lista de palestras. Cada palestra
precisa estar separada por uma ou mais novas linhas. Ex.:

```
$ echo "Writing Fast Tests Against Enterprise Rails 60min\n
Overdoing it in Python 45min\n
Lua for the Masses 30min\n
Ruby Errors from Mismatched Gem Versions 45min\n
Common Ruby Errors 45min\n
Rails for Python Developers lightning\n
Communicating Over Distance 60min\n
Accounting-Driven Development 45min\n
Woah 30min\n
Sit Down and Write 30min\n
Pair Programming vs Noise 45min\n
Rails Magic 60min\n
Ruby on Rails: Why We Should Move On 60min\n
Clojure Ate Scala (on my project) 45min\n
Programming in the Boondocks of Seattle 30min\n
Ruby vs. Clojure for Back-End Development 30min\n
Ruby on Rails Legacy App Maintenance 60min\n
A World Without HackerNews 30min\n
User Interface CSS in Rails Apps 30min" | ./ctm
```

## Rodando os testes

Para rodar os testes basta executar o seguinte comando:

```
$ mix test
```

# Design

O sistema foi desenvolvido para encaixar as palestras numa determinada janela
de tempo chamada de `boundaries` internamente. Esta janela pode ser fixa, onde
o momento de início e término são iguais, ou variável. No nosso caso, como temos
duas tracks de palestras em dois momentos diferentes cada, juntamos um total de
quatro tracks, duas fixas representando o período da manhã, e duas variáveis
para o período da tarde. O período de cada janela é representado por minutos, ou
seja, para um período que vai das 09:00AM às 12:00AM temos uma janela de
`{180, 180}`. Para um período variável das 01:00PM às 04:00PM-05:00PM temos a
janela `{180, 240}`.

Quando o sistema recebe a lista via `stdin` é realizado um parsing para extrair
o número de minutos de cada palestra o que acontece em `Ctm.Talks.make/1`. Em
seguida jogamos estas palestras mais os nosso `boundaries` para o
`Ctm.Orquestrator.run!/3` que vai, de acordo com cada conjunto de `boundaries`,
designar as palestras do período usando `Ctm.Queue.process/1`. Esta função
recebe um `struct` que representa a situação da fila de palestras sendo
analisada e executa um algoritmo que busca selecionar a ordem ideal de palestras
levando em vista seu tempo e o tempo disponível, bem como se a janela em que as
palestras estão sendo encaixadas é variável ou não.

Como o nome da função entrega, rodar `Ctm.Orquestrator.run!/3` vai levantar uma
excessão caso o sistema não seja capaz de encaixar todas as palestras nas
janelas designadas a contento. Os erros podem acontecer enquanto processamos
uma lista de palestras com `Ctm.Queue.process/1` ou finalmente no próprio
`Ctm.Orquestrator.run!/3` se ainda houver palestras sobressalentes após o
sistema conseguir encaixar palestras em todas as janelas disponíveis.

A função `Ctm.main/1` é responsável por juntar todas as partes e efetivamente
exibir o resultado na tela fazendo o ajuste dos horários de acordo com cada
palestra, período e track.

## Estado e imutabilidade

Elixir é uma linguagem funcional imutável. Isso quer dizer que não podemos
alterar os valores de estruturas o que torna a iteração pelos eventos na hora
do processamento um pouco mais complexo que numa linguagem imperativa.

Para facilitar este processo foi criado um `Agent` segurando o estado das
palestras disponíveis a cada iteração num processo diferente. Este `Agent` é
iniciado com a função `Ctm.Talks.start_link/2` e consumido como uma fila pela
função `Ctm.Queue.process/1`, que define novamente o estado do `Agent` ao mover
as palestras do pool de palestras disponíveis para o pool das palestras que
foram encaixadas numa determinada janela.

defmodule Ctm.Fixtures do
  @raw_input "Writing Fast Tests Against Enterprise Rails 60min\n\nOverdoing it in Python 45min\n\nLua for the Masses 30min\n\nRuby Errors from Mismatched Gem Versions 45min\n\nCommon Ruby Errors 45min\n\nRails for Python Developers lightning\n\nCommunicating Over Distance 60min\n\nAccounting-Driven Development 45min\n\nWoah 30min\n\nSit Down and Write 30min\n\nPair Programming vs Noise 45min\n\nRails Magic 60min\n\nRuby on Rails: Why We Should Move On 60min\n\nClojure Ate Scala (on my project) 45min\n\nProgramming in the Boondocks of Seattle 30min\n\nRuby vs. Clojure for Back-End Development 30min\n\nRuby on Rails Legacy App Maintenance 60min\n\nA World Without HackerNews 30min\n\nUser Interface CSS in Rails Apps 30min"

  @expected_output [
    {60, "Writing Fast Tests Against Enterprise Rails 60min"},
    {45, "Overdoing it in Python 45min"},
    {30, "Lua for the Masses 30min"},
    {45, "Ruby Errors from Mismatched Gem Versions 45min"},
    {45, "Common Ruby Errors 45min"},
    {5, "Rails for Python Developers lightning"},
    {60, "Communicating Over Distance 60min"},
    {45, "Accounting-Driven Development 45min"},
    {30, "Woah 30min"},
    {30, "Sit Down and Write 30min"},
    {45, "Pair Programming vs Noise 45min"},
    {60, "Rails Magic 60min"},
    {60, "Ruby on Rails: Why We Should Move On 60min"},
    {45, "Clojure Ate Scala (on my project) 45min"},
    {30, "Programming in the Boondocks of Seattle 30min"},
    {30, "Ruby vs. Clojure for Back-End Development 30min"},
    {60, "Ruby on Rails Legacy App Maintenance 60min"},
    {30, "A World Without HackerNews 30min"},
    {30, "User Interface CSS in Rails Apps 30min"}
  ]

  @first_processed_segment [
    {60, "Writing Fast Tests Against Enterprise Rails 60min"},
    {30, "Lua for the Masses 30min"},
    {45, "Ruby Errors from Mismatched Gem Versions 45min"},
    {45, "Common Ruby Errors 45min"}
  ]

  @processed_output [
    [
      {60, "Writing Fast Tests Against Enterprise Rails 60min"},
      {30, "Lua for the Masses 30min"},
      {45, "Ruby Errors from Mismatched Gem Versions 45min"},
      {45, "Common Ruby Errors 45min"}
    ],
    [
      {60, "Communicating Over Distance 60min"},
      {45, "Accounting-Driven Development 45min"},
      {45, "Pair Programming vs Noise 45min"},
      {45, "Clojure Ate Scala (on my project) 45min"},
      {45, "Overdoing it in Python 45min"}
    ],
    [
      {30, "Woah 30min"},
      {30, "Sit Down and Write 30min"},
      {60, "Rails Magic 60min"},
      {60, "Ruby on Rails: Why We Should Move On 60min"}
    ],
    [
      {30, "Programming in the Boondocks of Seattle 30min"},
      {30, "Ruby vs. Clojure for Back-End Development 30min"},
      {60, "Ruby on Rails Legacy App Maintenance 60min"},
      {30, "A World Without HackerNews 30min"},
      {30, "User Interface CSS in Rails Apps 30min"},
      {5, "Rails for Python Developers lightning"}
    ]
  ]

  @doc false
  def raw_input(), do: @raw_input

  @doc false
  def expected_output(), do: @expected_output

  @doc false
  def processed_output(), do: @processed_output

  @doc false
  def first_processed_segment(), do: @first_processed_segment
end

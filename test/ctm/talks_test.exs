defmodule Ctm.TalksTest do
  use ExUnit.Case

  alias Ctm.Talks

  import Ctm.Fixtures

  test "make/1" do
    assert Talks.make(raw_input()) == expected_output()
  end

  test "agent tests" do
    agent_name = :"test#{:rand.uniform(999_999)}"
    {:ok, pid} = Talks.start_link(expected_output(), name: agent_name)

    assert is_pid(pid)
    assert Talks.get(agent_name) == expected_output()

    [_ | talks] = expected_output()
    Talks.update(agent_name, talks)

    assert Talks.get(agent_name) == talks
  end
end

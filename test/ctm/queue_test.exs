defmodule Ctm.QueueTest do
  use ExUnit.Case

  alias Ctm.Queue

  import Ctm.Fixtures

  test "process just works as expected" do
    talks = expected_output()
    boundaries = {180, 180}
    state = %Queue{talks: talks, boundaries: boundaries}
    {:ok, %Queue{sequence: sequence}} = Queue.process(state)

    assert sequence == first_processed_segment()
  end

  test "less talks than needed" do
    talks = [
      {30, "ABC"},
      {30, "ABC"},
      {30, "ABC"},
      {30, "ABC"}
    ]

    state = %Queue{talks: talks, boundaries: {120, 180}}
    {:error, error} = Queue.process(state)

    assert error == "O total das palestras não preenche o tempo mínimo do evento."
  end

  test "can't fit talks" do
    talks = [
      {30, "ABC"},
      {30, "ABC"},
      {30, "ABC"},
      {30, "ABC"}
    ]

    state = %Queue{talks: talks, boundaries: {180, 180}}
    {:error, error} = Queue.process(state)

    assert error == "Não é possível encaixar as palestras dentro do tempo de evento estabelecido."
  end

  test "weird times" do
    talks = [
      {30, "ABC"},
      {30, "ABC"},
      {36, "ABC"},
      {38, "ABC"}
    ]

    state = %Queue{talks: talks, boundaries: {120, 130}}
    {:error, error} = Queue.process(state)

    assert error ==
             "O total de palestras excede o tempo do evento ou não existem palestras suficientes."
  end
end

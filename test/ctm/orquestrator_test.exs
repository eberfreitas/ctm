defmodule Ctm.OrquestratorTest do
  use ExUnit.Case

  alias Ctm.Orquestrator

  import Ctm.Fixtures

  test "process just works as expected" do
    talks = expected_output()
    boundaries = [{180, 180}, {180, 240}, {180, 180}, {180, 240}]

    assert Orquestrator.run(boundaries, talks) == processed_output()
  end

  test "exception" do
    talks = [
      {30, "ABC"},
      {30, "ABC"},
      {30, "ABC"}
    ]

    boundaries = [{60, 60}]

    assert_raise RuntimeError, fn ->
      Orquestrator.run!(boundaries, talks)
    end
  end
end
